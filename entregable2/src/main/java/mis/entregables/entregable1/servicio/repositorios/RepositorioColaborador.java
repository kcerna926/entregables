package mis.entregables.entregable1.servicio.repositorios;

import mis.entregables.entregable1.modelo.Colaborador;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioColaborador extends MongoRepository<Colaborador, String>,
RepositorioColaboradorExtendido{
}
