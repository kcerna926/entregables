package mis.entregables.entregable1.servicio.repositorios;

import mis.entregables.entregable1.modelo.Colaborador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class RepositorioColaboradorImpl implements RepositorioColaboradorExtendido {

    @Autowired
    MongoTemplate mongoTemplate;

    public void modificarColaborador(String numEmpleado, Colaborador colaborador) {
        final Query filtro = new Query();
        filtro.addCriteria(Criteria.where("_id").is(numEmpleado));

        final Update parche = new Update();

        ajustarCampo(parche, "numEmpleado", colaborador.numEmpleado);
        ajustarCampo(parche, "nombre", colaborador.nombre);
        ajustarCampo(parche, "apellidos", colaborador.apellidos);
        ajustarCampo(parche, "tipoDoc", colaborador.tipoDoc);
        ajustarCampo(parche, "numDoc", colaborador.numDoc);
        ajustarCampo(parche, "rol", colaborador.rol);

        this.mongoTemplate.updateFirst(filtro, parche, colaborador.getClass());
    }

    private void ajustarCampo(Update u, String nombre, String valor) {
        if(valor != null && !valor.trim().isEmpty())
            u.set(nombre, valor.trim());
    }
}
