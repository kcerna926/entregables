package mis.entregables.entregable1.servicio;

import mis.entregables.entregable1.modelo.Colaborador;
import mis.entregables.entregable1.servicio.repositorios.RepositorioColaborador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ServicioColaboradorImpl implements ServicioColaborador{

    @Autowired
    RepositorioColaborador repositorioColaborador;

    public Page<Colaborador> obtenerColaboradores(int pagina, int tamPagina) {
        return this.repositorioColaborador.findAll(PageRequest.of(pagina, tamPagina));
    }

    public void agregarColaborador(Colaborador colaborador) {
        this.repositorioColaborador.insert(colaborador);
    }

    public Colaborador obtenerColaborador(String numEmpleado) {
        final Optional<Colaborador> r = this.repositorioColaborador.findById(numEmpleado);
        return r.isPresent() ? r.get() : null;
    }

    public boolean existe(String numEmpleado) {
        return this.repositorioColaborador.existsById(numEmpleado);
    }
    public void reemplazarColaborador(String numEmpleado, Colaborador colaborador) {
        colaborador.numEmpleado = numEmpleado;
        this.repositorioColaborador.save(colaborador);
    }

    @Transactional
    public void eliminarColaboradorConTeescuchos(String numEmpleado) {
        //this.repos.borrarCuentasCliente(nroCliente);
        this.repositorioColaborador.deleteById(numEmpleado);
    }

    public void emparcharColaborador(String numEmpleado, Colaborador colaborador) {
        this.repositorioColaborador.modificarColaborador(numEmpleado, colaborador);
    }

}
