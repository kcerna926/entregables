package mis.entregables.entregable1.servicio;

import mis.entregables.entregable1.modelo.Colaborador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.Collections;
import java.util.List;

public interface ServicioColaborador {

    public Page<Colaborador> obtenerColaboradores(int pagina, int tamPagina);

    public void agregarColaborador(Colaborador colaborador);

    public Colaborador obtenerColaborador(String numEmpleado);

    public void reemplazarColaborador(String numEmpleado, Colaborador colaborador);

    public void eliminarColaboradorConTeescuchos(String numEmpleado);

    public void emparcharColaborador(String numEmpleado, Colaborador colaborador);

    public boolean existe(String numEmpleado);
}
