package mis.entregables.entregable1.servicio.repositorios;

import mis.entregables.entregable1.modelo.Colaborador;

public interface RepositorioColaboradorExtendido {
    public void modificarColaborador(String numEmpleado, Colaborador colaborador);
}
