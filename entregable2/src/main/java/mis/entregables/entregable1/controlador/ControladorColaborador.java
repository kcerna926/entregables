package mis.entregables.entregable1.controlador;


import mis.entregables.entregable1.modelo.Colaborador;
import mis.entregables.entregable1.servicio.ServicioColaborador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/teescuchos/v1/colaboradores")
public class ControladorColaborador {

    @Autowired
    ServicioColaborador servicioColaborador;

    @GetMapping
    public Page<Colaborador> obtenerColaboradores(
            @RequestParam (required = false) Integer numPagina,
            @RequestParam (required = false) Integer tamPagina
    ){
        // Controlar parámetros paginación.
        if(numPagina == null || numPagina < 0)
            numPagina = 0;
        if(tamPagina == null || tamPagina < 20)
            tamPagina = 20;
        if(tamPagina > 100)
            tamPagina = 100;
        return this.servicioColaborador.obtenerColaboradores(numPagina, tamPagina);
    }

    @PostMapping
    public ResponseEntity agregarColaborador(@RequestBody Colaborador colaborador){
        this.servicioColaborador.agregarColaborador(colaborador);
        final Link self = linkTo(methodOn(this.getClass()).obtenerColaborador(colaborador.numEmpleado))
                .withSelfRel();

        return ResponseEntity.ok().location(self.toUri()).build();
    }

    @GetMapping("/{numEmpleado}")
    public Colaborador obtenerColaborador(@PathVariable String numEmpleado) {
        return this.servicioColaborador.obtenerColaborador(numEmpleado);
    }

    @PutMapping("/{numEmpleado}")
    public void reemplazarColaborador(@PathVariable String numEmpleado, @RequestBody Colaborador colaboradorNuevo) {
        this.servicioColaborador.reemplazarColaborador(numEmpleado, colaboradorNuevo);
    }

    @DeleteMapping("/{numEmpleado}")
    public void eleminarColaborador(@PathVariable String numEmpleado) {
        this.servicioColaborador.eliminarColaboradorConTeescuchos(numEmpleado);
    }

    @PatchMapping("/{numEmpleado}")
    public void modificarColaborador(@PathVariable String numEmpleado, @RequestBody Colaborador colaborador) {
        this.servicioColaborador.emparcharColaborador(numEmpleado, colaborador);
    }



}
