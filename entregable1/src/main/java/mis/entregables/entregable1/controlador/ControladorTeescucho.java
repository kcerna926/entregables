package mis.entregables.entregable1.controlador;


import mis.entregables.entregable1.modelo.Teescucho;
import mis.entregables.entregable1.servicio.ServicioTeescucho;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/teescuchos")
@ExposesResourceFor(Teescucho.class)
public class ControladorTeescucho {

    @Autowired
    ServicioTeescucho servicioTeescucho;

    @Autowired
    EntityLinks entityLinks;

    public static class ResumenTeescucho {
        public String numEmpleado;
        public String tipoSolicitud;
    }

    @PostMapping
    public ResponseEntity agregarTeescucho(@RequestBody Teescucho teescucho){
        this.servicioTeescucho.agregarTeescucho(teescucho);
        return ResponseEntity
                .ok()
                .location(obtenerUrlTeescucho(teescucho).toUri())
                .build()
                ;
    }

    @GetMapping("/{numSolicitud}")
    public EntityModel<Teescucho> obtenerTeescucho(@PathVariable long numSolicitud) {

        final Teescucho t = buscarTeescuchoPorId(numSolicitud);
        final EntityModel<Teescucho> e = EntityModel.of(t);
        final Link enlaceEsteTeescucho = linkTo(methodOn(this.getClass()).obtenerTeescucho(numSolicitud)).withSelfRel();
        final Link enlaceTodosLosTeescuchos = linkTo(methodOn(this.getClass()).obtenerTeescuchos())
                .withRel("teescuchos").withTitle("Todos los teescuchos");
        final List<Link> listaEnlaces = Arrays.asList(
                enlaceEsteTeescucho,
                enlaceTodosLosTeescuchos
        );
        e.add(listaEnlaces);
        return e;
    }

    /*@GetMapping
    public CollectionModel<EntityModel<ResumenTeescucho>> obtenerTeescuchos() {

        List<Teescucho> tt = this.servicioTeescucho.obtenerTeescuchos();
        return CollectionModel.of(
                tt.stream().map(t -> crearRespuestaResumenTeescucho(t)).collect(Collectors.toList())
        ).add(linkTo(methodOn(this.getClass()).obtenerTeescuchos()).withSelfRel());
    }*/

    @GetMapping
    public List<Teescucho> obtenerTeescuchos(){
        return this.servicioTeescucho.obtenerTeescuchos();
    }


    @PutMapping("/{numSolicitud}")
    public ResponseEntity reemplazarTeescucho(@PathVariable long numSolicitud, @RequestBody Teescucho teescuchoNuevo) {

        Teescucho t = this.buscarTeescuchoPorId(numSolicitud);
        if (t == null) {
            return ResponseEntity.notFound().build();
        }
        servicioTeescucho.reemplazarTeescucho(numSolicitud, teescuchoNuevo);
        return new ResponseEntity<>("Teescucho actualizado correctamente.", HttpStatus.OK);
    }

    @DeleteMapping("/{numSolicitud}")
    public ResponseEntity eliminarTeescucho(@PathVariable long numSolicitud) {

        Teescucho t = servicioTeescucho.obtenerTeescucho(numSolicitud);
        if (t == null) {
            return ResponseEntity.notFound().build();
        }
        else {
            servicioTeescucho.eliminarTeescucho(numSolicitud);
            return new ResponseEntity<>("Teescucho eliminado correctamente.", HttpStatus.OK);
        }
    }

    @PatchMapping("/{numSolicitud}")
    public ResponseEntity  modificarTeescucho(@PathVariable long numSolicitud,
                                  @RequestBody Teescucho t) {
        final Teescucho tt = buscarTeescuchoPorId(numSolicitud);
        if (tt == null) {
            return ResponseEntity.notFound().build();
        }
        // Validación de los datos de entrada
        if(t.numEmpleado != null)
            tt.numEmpleado=t.numEmpleado;
        if(t.fechaCita != null)
            tt.fechaCita=t.fechaCita;
        if(t.tipoSolicitud != null)
            tt.tipoSolicitud=t.tipoSolicitud;
        if(t.fechaSolitud != null)
            tt.fechaSolitud=t.fechaSolitud;

        servicioTeescucho.reemplazarTeescucho(numSolicitud, tt);
        return ResponseEntity.ok(buscarTeescuchoPorId(numSolicitud));
    }

    @GetMapping("/{numSolicitud}/colaboradores")
    public ResponseEntity obtenerColaboradorDeSolicitud(@PathVariable long numSolicitud){
        final Teescucho t = servicioTeescucho.obtenerTeescucho(numSolicitud);
        final String idColaborador = t.numEmpleado;
        if (t == null) {
            return new ResponseEntity<>("TeEscucho no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (t.numEmpleado!=null)
            return new ResponseEntity<>("Empleado encontrado.", HttpStatus.OK);

        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private List<Link> crearEnlacesTeescucho(Teescucho t) {

        final Link teescuchos = linkTo(methodOn(this.getClass()).obtenerTeescuchos())
                .withRel("teescuchos").withTitle("Lista de todos los TeEscucho");

        return Arrays.asList(
                obtenerUrlTeescucho(t),
                teescuchos
        );
    }

    private EntityModel<ResumenTeescucho> crearRespuestaResumenTeescucho(Teescucho t) {
        final ResumenTeescucho r = new ResumenTeescucho();
        r.numEmpleado = t.numEmpleado;
        r.tipoSolicitud = t.tipoSolicitud;
        return EntityModel.of(r).add(obtenerUrlTeescucho(t));
    }
    private EntityModel<Teescucho> crearRespuestaTeescucho(Teescucho t) {
        return EntityModel.of(t).add(crearEnlacesTeescucho(t));
    }

    private Link obtenerUrlTeescucho(Teescucho t) {
        return this.entityLinks
                .linkToItemResource(t.getClass(), t.numSolicitud)
                .withSelfRel()
                .withTitle("Ver detalles")
                ;
    }

    private Teescucho buscarTeescuchoPorId(long numSolicitud) {
        final Teescucho t = this.servicioTeescucho.obtenerTeescucho(numSolicitud);
        if(t == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return t;
    }

}
