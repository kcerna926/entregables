package mis.entregables.entregable1.servicio;

import mis.entregables.entregable1.modelo.Colaborador;
import mis.entregables.entregable1.modelo.Teescucho;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class ServicioTeescucho {

    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<Long, Teescucho> teescuchos = new ConcurrentHashMap<Long, Teescucho>();

    public long agregarTeescucho(Teescucho teescucho) {
        final long numSolicitud = this.secuenciador.incrementAndGet();
        this.teescuchos.put(numSolicitud, teescucho);
        return numSolicitud;
    }

    public List<Teescucho> obtenerTeescuchos() {
        return this.teescuchos
                .entrySet()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());
    }


    public Teescucho obtenerTeescucho(long numSolicitud) {
        final Teescucho t = this.teescuchos.get(numSolicitud);
        return t;
    }

    public void reemplazarTeescucho(long numSolicitud, Teescucho t) {
        this.teescuchos.put(numSolicitud, t);
    }


    public void eliminarTeescucho(long numSolicitud) {
        this.teescuchos.remove(numSolicitud);
    }

    public String obtenerColaborador(long numSolicitud) {
        final Teescucho t = this.teescuchos.get(numSolicitud);
        return t.numEmpleado;
    }

}
