package mis.entregables.entregable1.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Teescucho {
    @JsonIgnore
    public long numSolicitud;
    public String numEmpleado;
    public String tipoSolicitud;
    public String fechaSolitud;
    public String fechaCita;

}
