package mis.entregables.entregable1.modelo;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Colaborador {

    @Id @JsonProperty(access= JsonProperty.Access.READ_ONLY)
    public String numEmpleado;
    public String nombre;
    public String apellidos;
    public String tipoDoc;
    public String numDoc;
    public String rol;

}
